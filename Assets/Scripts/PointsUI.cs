﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PointsUI : MonoBehaviour
{
    public float screenTime = 1f;
    public float speed = 5f;

    private int priorValue = 0;
    public TextMeshProUGUI pointsText;
    public TextMeshProUGUI comboText;

    private Vector3 startingPosition;
    private bool fadeOutHappening = false;

    private PlayerStats playerStats;
    private int pointsGained;

    void Start()
    {
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        startingPosition = GetComponent<RectTransform>().localPosition;
    }

    void Update()
    {
        pointsGained = playerStats.points - priorValue;
        if (pointsGained > 0)
        {
            pointsText.color = new Color (255, 255, 255, 1);
            comboText.color = new Color(255, 255, 255, 1);
            pointsText.text = ("+" + pointsGained.ToString());
            comboText.text = ("Combo x" + playerStats.combo.ToString());

            StartCoroutine(fadeOut());
        }

        if (fadeOutHappening)
        {
            GetComponent<RectTransform>().localPosition += Vector3.up * speed * Time.deltaTime;
            pointsText.color = new Color(255, 255, 255, pointsText.color.a - Time.deltaTime/screenTime);
            comboText.color = new Color(255, 255, 255, comboText.color.a - Time.deltaTime/screenTime);
        }

        priorValue = playerStats.points;
    }

    IEnumerator fadeOut()
    {
        pointsText.color = new Color(255, 255, 255, 1);
        comboText.color = new Color(255, 255, 255, 1);

        fadeOutHappening = true;
        yield return new WaitForSeconds(screenTime);

        pointsText.color = new Color(0, 0, 0, 0);
        comboText.color = new Color(0, 0, 0, 0);
        fadeOutHappening = false;
        GetComponent<RectTransform>().localPosition = startingPosition;


    }
}
