﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemy : MonoBehaviour
{
    private Animator anim;
    private GameObject followTarget;
    private CharacterController characterController;

    private GameController gameController;

    private Quaternion lookRotation;


    [Header("Unity Setup Fields")]
    public string playerTag = "Player";
    public string attackingTag = "attacking";
    public string runningTag = "running";
    public string castingTag = "casting";
    public string dieTag = "die";
    public string idleTag = "Idle";

    public float attackRange = 3f;
    public GameObject magicObject;
    public Transform magicSpawnTransform;
    public float timeBeforeDestroyAfterDying;
    public GameObject raycastOrigin;
    public Staff weaponObject;
    public Transform collectableSpawnTransform;


    [HideInInspector]
    public bool attackingFlag = false;
    [HideInInspector]
    public bool castingFlag = false;
    [HideInInspector]
    public bool idleFlag = true;
    [HideInInspector]
    public int spawnPointIndex; //this is defined by spawn script

    private EnemyCombatStats enemyStats;
    private bool dieAnimationDone = false;
    private GameObject rangedEnemySpawnManager;
    private EnemySpawnManager enemySpawnManager;


    // Start is called before the first frame update
    private void Start()
    {
        anim = GetComponent<Animator>();
        enemyStats = GetComponent<EnemyCombatStats>();
        characterController = GetComponent<CharacterController>();
        followTarget = GameObject.FindGameObjectWithTag(playerTag);
        gameController = FindObjectOfType<GameController>();
        rangedEnemySpawnManager = GameObject.FindGameObjectWithTag("RangedEnemySpawnManager");
        enemySpawnManager = rangedEnemySpawnManager.GetComponent<EnemySpawnManager>();


    }

    private void Update()
    {

        //Makes sure gravity is enabled at all times
        characterController.SimpleMove(Vector3.zero);

        //Code below decides which animation will play
        if (enemyStats.enemyDied)
        {
            Die();
        }

        else
        {
            if (!attackingFlag)
            {
                Turn();
            }

            if (TargetNear() && !attackingFlag)
            {
                Attack();
            }


            if (!TargetNear() && !attackingFlag & !castingFlag)
            {
                if (HasLineOfSightToPlayer())
                {
                    StartCast();

                }
                else if (!idleFlag)
                {
                    anim.SetTrigger(idleTag);
                    idleFlag = true;
                }

            }



        }

        //Destroys object if enemy has been dead for some time
        if (enemyStats.enemyDied)
            StartCoroutine(DestroyAfterTime());


    }

    private bool TargetNear()
    {
        bool reached = false;
        Vector3 distance = followTarget.transform.position - transform.position;
        
        if (distance.magnitude <= attackRange)
            return reached = true;
        else
            return reached = false;


    }


    private void Turn()
    {
        Vector3 dir = followTarget.transform.position - transform.position;
        lookRotation = Quaternion.LookRotation(dir);

        transform.eulerAngles = new Vector3(0, lookRotation.eulerAngles.y, 0);

    }

    private bool HasLineOfSightToPlayer()
    {
        bool lineOfSight = false;
        RaycastHit raycast;

        Vector3 dir = followTarget.transform.position - raycastOrigin.transform.position;
        Physics.Raycast(raycastOrigin.transform.position, dir, out raycast);

        if (raycast.transform == null)
        {

            return lineOfSight;

        }

        lineOfSight = (raycast.transform.gameObject.tag == playerTag);

        return lineOfSight;
    }

    private void StartCast()
    {
        attackingFlag = false;
        idleFlag = false;
        castingFlag = true;
        anim.SetTrigger(castingTag);
                       
    }

    private void Cast()
    {
        //called by animation event
        GameObject magic = (GameObject)Instantiate(magicObject, magicSpawnTransform.position, magicSpawnTransform.rotation);
        magic.GetComponent<EnemyMagic>().magicDamage = enemyStats.magicDamage;
        magic.GetComponent<EnemyMagic>().speed = enemyStats.magicSpeed;
        magic.GetComponent<EnemyMagic>().enemyParent = gameObject;

    }

    private void FinishCast()
    {
        //called by animation event

        castingFlag = false;

    }

    private void Attack()
    {
        castingFlag = false;
        idleFlag = false;

        attackingFlag = true;
        anim.SetTrigger(attackingTag);

    }

    private void FinishAttack()
    {
        //called by animation event
        attackingFlag = false;
        weaponObject.newAttack = true;

    }


    private void Die()
    {

        if (!dieAnimationDone)
        {
            followTarget.GetComponent<PlayerStats>().AddPointsAndKill();

            attackingFlag = false;
            idleFlag = false;
            castingFlag = false;
            anim.SetTrigger(dieTag);
            StartCoroutine(gameController.BuildCollectable(collectableSpawnTransform.position));
            gameController.numberOfRangedEnemiesInScene--;

            enemySpawnManager.busySpots[spawnPointIndex] = false;

            //allows enemy to fall properly
            characterController.enabled = false;

        }

        dieAnimationDone = true;

    }

    IEnumerator DestroyAfterTime()
    {
        yield return new WaitForSeconds(timeBeforeDestroyAfterDying);

        Destroy(gameObject);
    }
    

    private void OnDrawGizmosSelected()
    {

        Gizmos.DrawWireSphere(transform.position, attackRange);

    }



}
