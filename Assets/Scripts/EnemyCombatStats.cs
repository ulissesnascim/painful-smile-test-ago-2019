﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyCombatStats : MonoBehaviour
{
    public int maxHealthInt = 30;
    public int meleeDamage = 20;
    public int magicDamage = 50;
    public float magicSpeed = 2f;
    public float moveSpeed = 10f;
    public bool enemyDied = false;

    public int health;
    

    void Start()
    {
        health = maxHealthInt;
    }

    void Update()
    {
        if (health <= 0)
        {
            enemyDied = true;
        }

    }

    public void TakeDamage(int damage)
    {
        health -= damage;

    }


}
