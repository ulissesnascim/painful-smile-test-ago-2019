﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public GameObject sceneLoader;

    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    void Update()
    {
        
    }

    public void PlayGame()
    {
        sceneLoader.GetComponent<SceneLoader>().sceneToLoadString = "MainLevel";
        sceneLoader.SetActive(true);

    }

    public void QuitGame()
    {

        Application.Quit();

    }
}
