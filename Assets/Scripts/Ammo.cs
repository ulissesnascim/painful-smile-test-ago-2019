﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    private PlayerStats playerStats;
    private GameController gameController;
    public float turnSpeed = 3f;


    void Start()
    {
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        gameController = FindObjectOfType<GameController>();

    }

    void Update()
    {
        transform.Rotate(Vector3.down * Time.deltaTime * turnSpeed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerStats.RecoverAmmo(gameController.ammoRecoveryValue);
            Destroy(gameObject);

        }
               
    }

}
