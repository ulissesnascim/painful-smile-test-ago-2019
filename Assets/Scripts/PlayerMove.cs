﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [SerializeField] private string horizontalInput;
    [SerializeField] private string verticalInput;
    
    public CharacterController charController;

    private float normalSpeed = 10f;
    private float sprintSpeed = 15f;
    private float currentSpeed = 0;


    private void Start()
    {
        normalSpeed = PlayerStats.instance.movementSpeed;
        sprintSpeed = PlayerStats.instance.sprintSpeed;
        currentSpeed = normalSpeed;

    }

    void Update()
    {
        if (!GameController.instance.countdownFinished || GameController.instance.gameIsPaused)
            return;

        Vector3 horizontalMovement = transform.right * Input.GetAxis(horizontalInput);
        Vector3 verticalMovement = transform.forward * Input.GetAxis(verticalInput);

        if (Input.GetKey(KeyCode.LeftShift))
            currentSpeed = sprintSpeed;
        else
            currentSpeed = normalSpeed;

        charController.SimpleMove(Vector3.ClampMagnitude(verticalMovement + horizontalMovement, 1f) * currentSpeed);

    }
}
