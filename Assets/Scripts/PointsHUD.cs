﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class PointsHUD : MonoBehaviour
{
    private TextMeshProUGUI text;
    private PlayerStats playerStats;


    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();

    }

    void Update()
    {
        text.text = playerStats.points.ToString();


    }
}
