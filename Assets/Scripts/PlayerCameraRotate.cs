﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraRotate : MonoBehaviour
{
    [SerializeField] private string XMouseName, YMouseName;
    [SerializeField] private float maxY, minY;
    private float YClampAux = 0;
    public float mouseSensitivity;
    public Transform playerTransform;
    private GameController gameController;


    private void Awake()
    {

    }

    private void Start()
    {

        CursorLock();
        gameController = FindObjectOfType<GameController>();


    }

    void Update()
    {
       if (gameController.gameOver || gameController.gameIsPaused)
            return;

        Rotation();

    }

    private void Rotation()
    {
        float Xinput = Input.GetAxis(XMouseName) * mouseSensitivity * Time.deltaTime;

        float Yinput = Input.GetAxis(YMouseName) * mouseSensitivity * Time.deltaTime;

        playerTransform.Rotate(Vector3.up * Xinput);
        transform.Rotate(Vector3.left * Yinput);

        YClampAux += Yinput;

        if (YClampAux > 90f)
        {
            ClampRotationXToValue(270f);
            Yinput = 0;
            YClampAux = 90f;
        }
            
        else if (YClampAux < -90f)
        {
            ClampRotationXToValue(90f);
            Yinput = 0;
            YClampAux = -90f;
        }

    }

    private void ClampRotationXToValue(float aux)
    {
        Vector3 eulerRotation = transform.eulerAngles;
        eulerRotation.x = Mathf.Clamp(aux, minY, maxY);
        transform.eulerAngles = eulerRotation;

    }

    private void CursorLock()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
}
