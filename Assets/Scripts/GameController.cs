﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameController : MonoBehaviour
{
    public static GameController instance = null;

    [HideInInspector] public int countdownTime;
    [HideInInspector] public int matchTime;
    [HideInInspector] public bool gameIsPaused = false;
    [HideInInspector] public bool countdownFinished = false;
    [HideInInspector] public bool gameOver = false;
    [HideInInspector] public float currentMatchTime = 0;


    public GameObject heartPrefab;
    public GameObject ammoPrefab;
    public int heartRecoveryValue = 50;
    public int ammoRecoveryValue = 5;
    public float chanceOfHeart;

    public GameObject rangedEnemySpawnManager;
    public GameObject meleeEnemySpawnManager;
    public SettingsController settingsController;
    private float secondsToSpawn;
    public float firstSpawnTime;
    public float maxSecondsToSpawn;
    public float minSecondsToSpawn;
    public AnimationCurve secondsToSpawnCurve;
    public float comboForMinSpawnTime;
    private float spawnTimer = 0;
    private float comboBasedSpawnTime = 0;

    public int maxRangedEnemiesInScene;
    public int numberOfRangedEnemiesInScene = 0;

    private float SFXVolume = 0;
    private float musicVolume = 0;



    //allows customization of enemy skins
    public string skinTag = "Skin";

    public Material meleeBlueMaterial;
    public Material meleeGreenMaterial;
    public Material meleePurpleMaterial;
    public Material meleeRedMaterial;
    public Material meleeTealMaterial;
    public Material meleeYellowMaterial;

    public Material rangedBlueMaterial;
    public Material rangedGreenMaterial;
    public Material rangedPurpleMaterial;
    public Material rangedRedMaterial;
    public Material rangedTealMaterial;
    public Material rangedYellowMaterial;

    [HideInInspector] public Material[] meleeMaterialToUse = new Material[6];
    [HideInInspector] public Material[] rangedMaterialToUse;
    [HideInInspector] public Material meleeSelectedMaterial;
    [HideInInspector] public Material rangedSelectedMaterial;
    [HideInInspector] public int meleeMaterialIndex;
    [HideInInspector] public int rangedMaterialIndex;


    void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        LoadSettings();

        maxRangedEnemiesInScene = rangedEnemySpawnManager.GetComponent<EnemySpawnManager>().spawnPoints.Length - 1;

        secondsToSpawn = countdownTime + firstSpawnTime;

        meleeMaterialToUse[0] = meleeBlueMaterial;
        meleeMaterialToUse[1] = meleeGreenMaterial;
        meleeMaterialToUse[2] = meleePurpleMaterial;
        meleeMaterialToUse[3] = meleeRedMaterial;
        meleeMaterialToUse[4] = meleeTealMaterial;
        meleeMaterialToUse[5] = meleeYellowMaterial;

        rangedMaterialToUse = new Material[6];
        rangedMaterialToUse[0] = rangedBlueMaterial;
        rangedMaterialToUse[1] = rangedGreenMaterial;
        rangedMaterialToUse[2] = rangedPurpleMaterial;
        rangedMaterialToUse[3] = rangedRedMaterial;
        rangedMaterialToUse[4] = rangedTealMaterial;
        rangedMaterialToUse[5] = rangedYellowMaterial;

        meleeSelectedMaterial = meleeMaterialToUse[meleeMaterialIndex];
        rangedSelectedMaterial = rangedMaterialToUse[rangedMaterialIndex];


        Time.timeScale = 1;

    }

    void Update()
    {
        spawnTimer += Time.deltaTime;
        currentMatchTime += Time.deltaTime;


        //enemy spawn times dynamically change as the player gets higher combos
        if (spawnTimer >= secondsToSpawn)
        {
            ChooseEnemyToSpawn();
            comboBasedSpawnTime = Mathf.Clamp(maxSecondsToSpawn - secondsToSpawnCurve.Evaluate(PlayerStats.instance.combo/comboForMinSpawnTime) * (maxSecondsToSpawn - minSecondsToSpawn), minSecondsToSpawn, maxSecondsToSpawn);
            secondsToSpawn = comboBasedSpawnTime;

            spawnTimer = 0;
        }

    }

    public IEnumerator BuildCollectable(Vector3 positionToBuild)
    {

        yield return new WaitForSeconds(1f);

        if (Random.Range(0, 1f) < chanceOfHeart)
        {
            Instantiate(heartPrefab, new Vector3 (positionToBuild.x, positionToBuild.y - 1.42f, positionToBuild.z), Quaternion.identity);

        }
        else
        {

            Instantiate(ammoPrefab, positionToBuild, Quaternion.identity);

        }

    }

    void ChooseEnemyToSpawn()
    {
        

        if (numberOfRangedEnemiesInScene >= maxRangedEnemiesInScene)
            meleeEnemySpawnManager.GetComponent<EnemySpawnManager>().spawnNow = true;

        else
        {
            if (Random.Range(0, 1f) < 0.65f)
                meleeEnemySpawnManager.GetComponent<EnemySpawnManager>().spawnNow = true;

            else
            {
                rangedEnemySpawnManager.GetComponent<EnemySpawnManager>().spawnNow = true;

                numberOfRangedEnemiesInScene++;
            }

        }
        


    }

    private void LoadSettings()
    {
        matchTime = PlayerPrefs.GetInt(settingsController.matchTimePrefs, settingsController.defaultMatchTime);
        countdownTime = PlayerPrefs.GetInt(settingsController.countdownPrefs, settingsController.defaultCountdown);
        SFXVolume = PlayerPrefs.GetFloat(settingsController.SFXVolumePrefs, settingsController.defaultSFXVolume);
        musicVolume = PlayerPrefs.GetFloat(settingsController.musicVolumePrefs, settingsController.defaultMusicVolume);
    }
}
