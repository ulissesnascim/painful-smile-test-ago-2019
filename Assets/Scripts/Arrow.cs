﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public string enemyTag = "Enemy";
    public Vector3 increasedColliderAfterStopping;
    public GameObject trail;
    public float trailMinVelocity;

    public float minForce = 10f;
    public float maxForce = 30f;
    public float minTimePressed = 0.25f;
    public float maxTimePressed = 0.75f;
    private float timeForceMultiplier = 0;
    private float force = 30f;

    private bool flying = false;
    private bool enemyHit = false;
    private bool enemyHitDied = false;
    private bool hitButNotEnemy = false;
    private EnemyCombatStats enemyHitStats;
    private int arrowDamage = 0;
    private PlayerStats playerStats;
    private CharacterController playerCharacterController;
    private BoxCollider collider;
    private GameObject player;


    private Vector3 positionWhenEnemyHit;
    private Quaternion rotationWhenEnemyHit;
    private bool rotationFixed = false;
    private bool positionFixed = false;


    private Rigidbody rb;

    Vector3 rotation;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();

        player = GameObject.FindGameObjectWithTag("Player");
        playerCharacterController = player.GetComponent<CharacterController>();
        playerStats = player.GetComponent<PlayerStats>();
        arrowDamage = playerStats.playerArrowDamage;
        collider = GetComponentInChildren<BoxCollider>();
        enemyHitStats = null;


    }

    private void Start()
    {
        Physics.IgnoreCollision(collider, playerCharacterController, true);
    }

    private void Update()
    {
        if (enemyHitStats != null)
            enemyHitDied = enemyHitStats.enemyDied;

        //makes sure arrow is correctly angled when enemy hit
        if (!hitButNotEnemy)
        {
            if (!enemyHit)
                rotationWhenEnemyHit = transform.rotation;
            else if (!rotationFixed)
            {
                transform.rotation = rotationWhenEnemyHit;
                rotationFixed = true;
            }
        }

        //makes sure arrow is correctly positioned when enemy hit
        if (!hitButNotEnemy)
        {
            if (!enemyHit)
                positionWhenEnemyHit = transform.position;
            else if (!positionFixed)
            {
                transform.position = positionWhenEnemyHit;
                positionFixed = true;
            }
        }


        if (flying)
        {
            transform.rotation = Quaternion.LookRotation(rb.velocity);

            if (rb.velocity.magnitude > trailMinVelocity)
                trail.SetActive(true);
            else
                trail.SetActive(false);
                                    
        }

        if (enemyHit && enemyHitStats.enemyDied)
        {

            collider.enabled = true;
            rb.useGravity = true;
            //rb.constraints = RigidbodyConstraints.None;


        }


        //allows player to take arrow back after it hits something
        if (!flying && (enemyHitDied || hitButNotEnemy))
        {
            collider.isTrigger = true;
            collider.size = increasedColliderAfterStopping;
            Physics.IgnoreCollision(collider, playerCharacterController, false);
            collider.enabled = true;

        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (flying = true)
        {
            flying = false;

            rb.isKinematic = true;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;

            transform.rotation = rotationWhenEnemyHit;

            rb.useGravity = false;


            if (collision.gameObject.tag == enemyTag)
            {
                transform.position = positionWhenEnemyHit;

                transform.SetParent(collision.gameObject.transform);
                enemyHitStats = collision.gameObject.GetComponent<EnemyCombatStats>();
                enemyHitStats.TakeDamage(arrowDamage);
                enemyHit = true;

            }
            else
                hitButNotEnemy = true;

            collider.enabled = false;
            trail.SetActive(false);


        }
                              
    }

    private void OnTriggerEnter(Collider collision)
    {

        if (collision.gameObject == player)
        {
            playerStats.RecoverAmmo(1);

            Destroy(gameObject);
        }

        
    }

    public void SetArrowForce(float timePressed)
    {
        rb.isKinematic = false;
        rb.useGravity = true;
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        timeForceMultiplier = (maxForce - minForce) / (maxTimePressed - minTimePressed);
        timePressed = Mathf.Clamp(timePressed, minTimePressed, maxTimePressed);

        force = Mathf.Min(minForce + (timePressed - minTimePressed) * timeForceMultiplier, maxForce);

        rb.velocity = transform.forward * force;
        flying = true;



    }

}
