﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    [SerializeField] private KeyCode shootKey;

    public Transform arrowSpawnTransform;
    public GameObject arrowPrefab;
    public GameObject playerBow;

    private PlayerStats playerStats;
    private GameController gameController;
    private GameObject arrow;
    private Arrow arrowScript;

    private bool arrowSet = false;
    private float timePressed = 0;


    private void Start()
    {
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        gameController = FindObjectOfType<GameController>();

    }


    void Update()
    {
        if (gameController.gameOver || gameController.gameIsPaused)
            return;

        if (Input.GetKeyDown(shootKey))
        {
            if (playerStats.ammo > 0)
            {
                ArrowSpawn();  
                playerStats.ammo -= 1;
            }
        }
        
        if (Input.GetKey(shootKey))
            timePressed += Time.deltaTime;


        if (Input.GetKeyUp(shootKey))
        {
            if (arrowSet)
            {
                ArrowRelease();
            }

        }


    }

    private void LateUpdate()
    {
        if (!Input.GetKey(shootKey))
            timePressed = 0;
    }

    private void ArrowSpawn()
    {

        arrow = (GameObject)Instantiate(arrowPrefab, arrowSpawnTransform.position, arrowSpawnTransform.rotation);
        arrowScript = arrow.GetComponent<Arrow>();
        arrow.transform.parent = playerBow.transform;
        arrowSet = true;

    }

    private void ArrowRelease()
    {
        if (arrow)
        {
            arrow.transform.parent = null;
            arrowScript.SetArrowForce(timePressed);
            arrowSet = false;

        }
            


    }
}
