﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Staff : MonoBehaviour
{
    private int damageGiven = 0;

    [HideInInspector] public bool newAttack = true;
    public bool _attackingFlag;

    private RangedEnemy enemyParent;


    void Start()
    {
        damageGiven = GetComponentInParent<EnemyCombatStats>().meleeDamage;
        enemyParent = GetComponentInParent<RangedEnemy>();

    }

    // Update is called once per frame
    void Update()
    {
        _attackingFlag = enemyParent.attackingFlag;

    }


    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            if (!GetComponentInParent<EnemyCombatStats>().enemyDied)
                PlayerHit(other);

        }

    }

    private void PlayerHit(Collider _other)
    {
        if (newAttack)
        {
            _other.gameObject.GetComponent<PlayerStats>().TakeDamage(damageGiven, enemyParent.transform);

            //to avoid damaging player twice in the same attack:
            newAttack = false;

        }

    }
}


