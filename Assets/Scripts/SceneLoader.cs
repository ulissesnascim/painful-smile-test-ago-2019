﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public string sceneToLoadString;
    public GameObject mainMenuPanel;
    public Slider loadingSlider;

    private AsyncOperation operation;

    private void OnEnable()
    {

        mainMenuPanel.SetActive(false);
        operation = SceneManager.LoadSceneAsync(sceneToLoadString);


    }

    private void Update()
    {
        if (!operation.isDone)
        {
            loadingSlider.value = operation.progress;

        }


    }


}
