﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class SkinSelector : EditorWindow
{
    public string[] materialsText = new string[] { "blue", "green", "purple", "red", "teal", "yellow"};
    public int meleeIndex = 0;
    public int rangedIndex = 0;


    [MenuItem("Window/Select Skins")]

    public static void ShowWindow()
    {
        GetWindow<SkinSelector>("Select Skins");
        
    }

    private void OnGUI()
    {
        GUILayout.Label("Melee Enemy color");
        meleeIndex = EditorGUILayout.Popup(meleeIndex, materialsText);

        GUILayout.Label("Ranged Enemy color");
        rangedIndex = EditorGUILayout.Popup(rangedIndex, materialsText);

        if (GUILayout.Button("Confirm"))
        {
            SendInfoToManager();
        }
    }

    void SendInfoToManager()
    {
        GameController gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

        gameController.meleeMaterialIndex = meleeIndex;
        gameController.rangedMaterialIndex = rangedIndex;

    }

}
