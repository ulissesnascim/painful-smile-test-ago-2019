﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    public CharacterController charController;

    private bool isJumping = false;

    [SerializeField] private KeyCode jumpKey;
    [SerializeField] private float jumpMultiplier;
    [SerializeField] private AnimationCurve jumpCurve;
        
    void Update()
    {
        if (Input.GetKeyDown(jumpKey) && !isJumping)
        {
            isJumping = true;
            StartCoroutine(Jumping());
        }
    }

    private IEnumerator Jumping()
    {
        float timeJumping = 0;

        do
        {
            float force = jumpCurve.Evaluate(timeJumping);

            charController.Move(Vector3.up * force * jumpMultiplier * Time.deltaTime);
            timeJumping += Time.deltaTime;

            yield return null;
        } while (!charController.isGrounded && charController.collisionFlags != CollisionFlags.Above);

        isJumping = false;        
        
    }
}
