﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroy : MonoBehaviour
{

    void Start()
    {
        
    }

    void Update()
    {
        if (!GetComponent<ParticleSystem>().IsAlive())
            Destroy(gameObject);
    }
}
