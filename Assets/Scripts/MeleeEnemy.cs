﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemy : MonoBehaviour
{
    private Animator anim;
    private float timeAttacking;
    private GameObject followTarget;

    private GameController gameController;

    [Header("Unity Setup Fields")]
    public string playerTag = "Player";
    public string attackingTag = "attacking";
    public string runningTag = "running";
    public string dieTag = "die";
    public string playerArrowTag = "Player Arrow";
    public Sword weaponObject;
    public float attackRange = 0.75f;
    public Transform collectableSpawnTransform;



    public bool attackingFlag = false;
    [HideInInspector]
    public bool runningFlag = false;


    private EnemyCombatStats enemyStats;
    private bool dieAnimationDone = false;
    private CharacterController characterController;



    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        followTarget = GameObject.FindGameObjectWithTag(playerTag);
        enemyStats = GetComponent<EnemyCombatStats>();
        gameController = FindObjectOfType<GameController>();
        characterController = GetComponent<CharacterController>();

    }

    void Update()
    {

        //Makes sure gravity is enabled at all times
        characterController.SimpleMove(Vector3.zero);
        
        if (enemyStats.enemyDied)
        {
            Die();
        }

        else
        {
            if (!attackingFlag)
                Turn();

            if (!TargetReached() && !attackingFlag)
            {
                Walk();
            }


            if (TargetReached() && !attackingFlag)
            {
                Attack();

            }



        }
    }


    bool TargetReached()
    {
        bool reached = false;
        Vector3 distance = followTarget.transform.position - transform.position;

        if (distance.magnitude <= attackRange)
            return reached = true;
        else
            return reached = false;

        
    }


    void Turn ()
    {


        Vector3 dir = followTarget.transform.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);

        transform.eulerAngles = new Vector3(0, lookRotation.eulerAngles.y, 0);



    }

    void Walk()
    {


        if (!runningFlag)
            anim.SetTrigger(runningTag);

        runningFlag = true;

        characterController.Move(transform.forward * enemyStats.moveSpeed * Time.deltaTime);

    }

    private void Attack()
    {
        attackingFlag = true;
        runningFlag = false;

        anim.SetTrigger(attackingTag);
    }

    private void FinishAttack()
    {
        //called by animation event
        attackingFlag = false;
        weaponObject.newAttack = true;

    }


    void Die()
    {

        if (!dieAnimationDone)
        {
            

            followTarget.GetComponent<PlayerStats>().AddPointsAndKill();

            attackingFlag = false;
            runningFlag = false;

            anim.SetTrigger(dieTag);

            StartCoroutine(gameController.BuildCollectable(collectableSpawnTransform.position));

            //allows enemy to fall properly
            characterController.center = new Vector3(0, 1.2f, 0);
            StartCoroutine(TurnOffCharController());

        }

        dieAnimationDone = true;

    }

    private IEnumerator TurnOffCharController()
    {
        yield return new WaitForSeconds(1f);
        characterController.enabled = false;

    }


    private void OnDrawGizmosSelected()
    {

        Gizmos.DrawWireSphere(transform.position, attackRange);

    }



}
