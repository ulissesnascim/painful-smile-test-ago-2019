﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthUI : MonoBehaviour
{
    public int lowHealthMax = 25;
    public int mediumHealthMax = 50;
    public Color lowHealthColor;
    public Color mediumHealthColor;
    public Color highHealthColor;

    private TextMeshProUGUI text;
    private PlayerStats playerStats;

    public GameObject popUpPrefab;

    void Start()
    {
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        text = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        text.text = playerStats.health.ToString();

        if (playerStats.health <= lowHealthMax)
        {
            text.color = lowHealthColor;
        }
        else if (playerStats.health <= mediumHealthMax)
        {
            text.color = mediumHealthColor;
        }
        else
        {
            text.color = highHealthColor;
        }
    }

    public void ActivateHealthPopUp(int healthEarned)
    {

        GameObject popUp = (GameObject)Instantiate(popUpPrefab, transform.position, Quaternion.identity);
        popUp.transform.SetParent(transform.parent.transform);
        TextMeshProUGUI popUpText = popUp.GetComponent<TextMeshProUGUI>();

        if (healthEarned < 0)
        {
            popUpText.color = lowHealthColor;
            popUpText.text = healthEarned.ToString();
        }
        else
        {
            popUpText.color = highHealthColor;
            popUpText.text = "+" + healthEarned.ToString();
        }

        popUp.GetComponent<Animator>().SetInteger("amount", healthEarned);

        Destroy(popUp, 5f);

    }
}
