﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemySpot : MonoBehaviour
{
    //this is used by the spawn manager
    public int index;

    private void OnDrawGizmos()
    {

        Gizmos.color = Color.yellow;

        Gizmos.DrawWireSphere(transform.position, 3f);


    }
}
