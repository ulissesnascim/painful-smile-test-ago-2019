﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class CountdownBeforeGame : MonoBehaviour
{
    public GameObject controlsText;
    private GameController gameController;
    private int currentTime;
    private float timer;


    void Start()
    {
        gameController = FindObjectOfType<GameController>();
        gameController.gameIsPaused = true;

    }

    void Update()
    {
        timer += Time.deltaTime;

        currentTime = Mathf.CeilToInt(gameController.countdownTime - timer);
        GetComponent<TextMeshProUGUI>().text = currentTime.ToString();

        if (currentTime <= 0)
        {
            GetComponent<TextMeshProUGUI>().text = "GO";
            gameController.gameIsPaused = false;
            gameController.countdownFinished = true;


        }

        if (timer > (gameController.countdownTime + 1))
        {
            Destroy(controlsText);
            Destroy(gameObject);


        }


    }
}
