﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public KeyCode pauseKey;
    public GameObject pauseMenuCanvas;
    public GameObject gameplayCanvas;

    private void Start()
    {

    }

    void Update()
    {

        if (Input.GetKeyDown(pauseKey) && GameController.instance.countdownFinished)
        {
            if (GameController.instance.gameIsPaused == false)
            {
                PauseGame();
            }
            else
                Resume();

        }

    }

    public void Resume()
    {
        Time.timeScale = 1;
        pauseMenuCanvas.SetActive(false);
        gameplayCanvas.SetActive(true);
        GameController.instance.gameIsPaused = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;


    }

    public void PauseGame()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        pauseMenuCanvas.SetActive(true);
        gameplayCanvas.SetActive(false);

        GameController.instance.gameIsPaused = true;
        Time.timeScale = 0;
               
    }

    public void QuitToMainMenu()
    {
        SceneManager.LoadScene("StartMenu");
    }

}
