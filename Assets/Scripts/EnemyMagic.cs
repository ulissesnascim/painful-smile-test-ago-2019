﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMagic : MonoBehaviour
{
    public string playerTag = "Player";
    public GameObject enemyParent;

    //exact time between the Cast and FinishCast animation events under the cast animation
    public float timeToCharge = 42/60;

    public float maxParticleScale = 2f;
    public float maxLightIntensity = 4.5f;
    public float maxLightRange = 1.2f;

    public Light pointLight;
    public ParticleSystem particles;

    //these attributes are defined by enemy combat stats in the prefab
    [HideInInspector]
    public int magicDamage = 0;
    [HideInInspector]
    public float speed = 0;

    public float timeBeforeDestroy = 10f;

    private Vector3 dir;
    private bool playerHit = false;
    private PlayerStats playerStats;
    private GameObject magicTarget;

    private float timeCharging = 0;
    private bool chargedFlag = false;


    void Start()
    {
        playerStats = GameObject.FindGameObjectWithTag(playerTag).GetComponent<PlayerStats>();
        magicTarget = GameObject.FindGameObjectWithTag("MagicTarget");
        StartCoroutine(DestroyAfterTime());


    }

    void Update()
    {

        if (!chargedFlag)
            Charge();
        else
        {
            
            Movement();

        }
    }

    IEnumerator DestroyAfterTime()
    {
        yield return new WaitForSeconds(timeBeforeDestroy);
        Destroy(gameObject);

    }

    void Charge()
    {
        timeCharging += Time.deltaTime;
        
        float ratio = (timeCharging / timeToCharge);
        float scaleThisFrame = ratio * maxParticleScale;
        float rangeThisFrame = ratio * maxLightRange;
        float intensityThisFrame = ratio * maxLightIntensity;

        particles.transform.localScale = new Vector3 (ratio, ratio, ratio);
        pointLight.range = rangeThisFrame;
        pointLight.intensity = intensityThisFrame;

        if (timeCharging >= timeToCharge)
        {
            dir = magicTarget.transform.position - transform.position;

            chargedFlag = true;
        }

    }


    void Movement ()
    {

        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == playerTag)
        {

            playerStats.TakeDamage(magicDamage, enemyParent.transform);
            playerHit = true;


        }

        if (collision.gameObject.GetComponent<RangedEnemy>() == null)
            Destroy(gameObject);



    }


}
