﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDie : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        int health = GetComponent<PlayerStats>().health;

        if (health <= 0)
        {
            Die();

        }

    }
    
    void Die()
    {
        Destroy(GameObject.FindGameObjectWithTag("Bow"));

    }

}
