﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;




public class GameOver : MonoBehaviour
{
    public TextMeshProUGUI combo;
    public TextMeshProUGUI points;
    public TextMeshProUGUI kills;
    public TextMeshProUGUI time;

    public GameObject gameOverCanvas;
    public GameObject HUDCanvas;
    public GameObject gameplayCanvas;



    void Update()
    {
        if (PlayerStats.instance.health <= 0 || GameController.instance.currentMatchTime > GameController.instance.matchTime * 60)
        {
            if (!GameController.instance.gameOver)
            {
                FinishMatch();

            }

        }



    }

    public void FinishMatch()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        gameOverCanvas.SetActive(true);
        HUDCanvas.SetActive(true);
        gameplayCanvas.SetActive(false);


        combo.text = "Max Combo Achieved: " + PlayerStats.instance.maxCombo;
        kills.text = "Kills: " + PlayerStats.instance.kills;
        points.text = "Points: " + PlayerStats.instance.points;
        time.text = "Minutes Survived: " + Mathf.FloorToInt(GameController.instance.currentMatchTime/60).ToString();

        GameController.instance.gameOver = true;
        GameController.instance.gameIsPaused = true;

        Time.timeScale = 0;


    }

    public void QuitToMainMenu()
    {
        SceneManager.LoadScene("StartMenu");


    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);


    }
}
