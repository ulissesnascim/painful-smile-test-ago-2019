﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AmmoUI : MonoBehaviour
{
    public int lowAmmoMax = 5;
    public Color lowAmmoColor;

    private PlayerStats playerStats;
    private TextMeshProUGUI text;

    public GameObject popUpPrefab;

    void Start()
    {
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        text = GetComponent<TextMeshProUGUI>();

    }

    void Update()
    {
        text.text = playerStats.ammo.ToString();

        if (playerStats.ammo <= lowAmmoMax)
        {
            text.color = lowAmmoColor;
        }
        else
        {
            text.color = Color.black;

        }
    }

    public void ActivateAmmoPopUp(int ammoEarned)
    {
        GameObject popUp = (GameObject)Instantiate(popUpPrefab, transform.position, Quaternion.identity);
        popUp.transform.SetParent(transform.parent.transform);
        popUp.GetComponent<TextMeshProUGUI>().text = "+" + ammoEarned.ToString();
        Destroy(popUp, 5f);
    }

}
