﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsController : MonoBehaviour
{
    [Header ("Set Default Settings (change prefab only)")]
    public int defaultCountdown;
    public int defaultMatchTime;
    public float defaultSFXVolume;
    public float defaultMusicVolume;

    [Header("Unity Setup Fields - PlayerPrefs")]
    public string musicVolumePrefs = "Music";
    public string SFXVolumePrefs = "SFX";
    public string matchTimePrefs = "MatchTime";
    public string countdownPrefs = "Countdown";

}
