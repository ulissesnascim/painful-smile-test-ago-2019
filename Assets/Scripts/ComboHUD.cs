﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ComboHUD : MonoBehaviour
{
    
    void Update()
    {
        GetComponent<TextMeshProUGUI>().text = "Combo x" + PlayerStats.instance.combo;
    }

}
