﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnManager : MonoBehaviour
{
    public GameObject enemyPrefab;
    public GameObject spawnParticleEffect;
    public bool[] busySpots;


    public Material meleeEnemyMaterial;
    public Material rangedEnemyMaterial;
    
    public Transform[] spawnPoints;
    private int numberOfSpawnPoints;
    [HideInInspector] public bool spawnNow = false;
    private Transform priorSelected;
    private int random = 0;
    private GameController gameController;


    void Awake()
    {
        gameController = FindObjectOfType<GameController>();


        spawnPoints = GetComponentsInChildren<Transform>();
        numberOfSpawnPoints = spawnPoints.Length;
        busySpots = new bool[numberOfSpawnPoints];

        for (int i = 0; i == numberOfSpawnPoints-1; i++)
        {
            busySpots[i] = false;
            spawnPoints[i].gameObject.GetComponent<RangedEnemySpot>().index = i;
        }


    }



    void Update()
    {
        
        if (spawnNow == true)
        {

            SpawnEnemy();
            spawnNow = false;

        }


    }

    Transform SelectSpawnPoint()
    {
        Transform selected;

        random = Mathf.RoundToInt(Random.Range(1, numberOfSpawnPoints));
        selected = spawnPoints[random];

        //checks whether the spawn point is busy with another enemy
        if (enemyPrefab.GetComponent<RangedEnemy>() != null)
        {

            while (busySpots[random] == true)
            {

                random = Mathf.RoundToInt(Random.Range(1, numberOfSpawnPoints));
                selected = spawnPoints[random];

            }
            busySpots[random] = true;
        }



        priorSelected = selected;

        return selected;

    }


    void SpawnEnemy()
    {
        Transform selectedSpawnPoint = SelectSpawnPoint();
        GameObject enemy = (GameObject)Instantiate(enemyPrefab, selectedSpawnPoint.position, selectedSpawnPoint.rotation);
        Instantiate(spawnParticleEffect, selectedSpawnPoint.position, selectedSpawnPoint.rotation);

        meleeEnemyMaterial = gameController.meleeSelectedMaterial;
        rangedEnemyMaterial = gameController.rangedSelectedMaterial;

        if (enemy.GetComponent<MeleeEnemy>() != null)
        {
            SkinnedMeshRenderer[] renderer = enemy.GetComponentsInChildren<SkinnedMeshRenderer>();

            int aux = 0;

            do
            {
                if (renderer[aux].gameObject.tag == gameController.skinTag)
                {

                    renderer[aux].sharedMaterial = gameController.meleeSelectedMaterial;

                }

                aux++;
            } while (aux < renderer.Length);
        }

        if (enemy.GetComponent<RangedEnemy>() != null)
        {
            enemy.GetComponent<RangedEnemy>().spawnPointIndex = random;

            SkinnedMeshRenderer[] renderer = enemy.GetComponentsInChildren<SkinnedMeshRenderer>();

            int aux = 0;

            do
            {
                if (renderer[aux].gameObject.tag == gameController.skinTag)
                {

                    renderer[aux].material = gameController.rangedSelectedMaterial;


                }

                aux++;
            } while (aux < renderer.Length);

        }
    }


}
