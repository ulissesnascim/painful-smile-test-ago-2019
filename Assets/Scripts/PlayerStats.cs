﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerStats : MonoBehaviour
{
    public static PlayerStats instance = null;

    [Header("Gameplay Stats")]
    public int maxHealthInt = 100;
    public int startingAmmo = 10;
    public int pointsPerKill = 100;
    public int playerArrowDamage = 10;
    public float movementSpeed = 10f;
    public float sprintSpeed = 15f;


    public int health;
    public int ammo;
    public int points = 0;
    public int kills = 0;
    public int combo = 0;
    public int maxCombo = 0;

    [Header("Unity Setup Fields")]
    public GameObject ammoUI;
    public GameObject healthUI;
    public GameObject comboUI;
    public GameObject damageIndicator;
    public int minComboToShowHUD = 5;

    void Awake()
    {

        health = maxHealthInt;
        ammo = startingAmmo;

        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

    }

    void Update()
    {
        ClampAttributes();

        comboUI.SetActive(combo >= minComboToShowHUD);

    }

    private void ClampAttributes()
    {
        ammo = Mathf.Clamp(ammo, 0, 999999);
        health = Mathf.Clamp(health, 0, maxHealthInt);
        combo = Mathf.Clamp(combo, 0, 999999);


    }

    public void TakeDamage(int damage, Transform damageAgentTransform)
    {
        health -= damage;
        combo = 0;
        healthUI.GetComponent<HealthUI>().ActivateHealthPopUp(-damage);
        StartCoroutine(ShowDamageIndicator(damageAgentTransform));

    }

    private IEnumerator ShowDamageIndicator(Transform _damageAgentTransform)
    {
        damageIndicator.SetActive(true);
        Vector3 lookRotation = Quaternion.LookRotation(_damageAgentTransform.position - transform.position).eulerAngles;
        damageIndicator.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, lookRotation.y - transform.eulerAngles.y);

        yield return new WaitForSeconds(0.3f);
        damageIndicator.SetActive(false);


    }

    public void Heal(int healValue)
    {
        health += healValue;
        healthUI.GetComponent<HealthUI>().ActivateHealthPopUp(healValue);
    }

    public void AddPointsAndKill()
    {
        combo += 1;
        kills += 1;
        points += pointsPerKill * combo;
        maxCombo = Mathf.Max(maxCombo, combo);
    }

    public void RecoverAmmo(int amount)
    {
        ammo += amount;
        ammoUI.GetComponent<AmmoUI>().ActivateAmmoPopUp(amount);
    }
    
}
