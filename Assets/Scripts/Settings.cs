﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;
using TMPro;

public class Settings : MonoBehaviour
{
    public Slider matchTimeSlider;
    public Slider countdownTimeSlider;
    public Slider musicSlider;
    public Slider SFXSlider;

    public TextMeshProUGUI matchTimeText;
    public TextMeshProUGUI countdownText;

    public SettingsController settingsController;

    private void OnEnable()
    {
        Load();
    }

    private void Update()
    {

        TextChange();

    }

    private void TextChange()
    {
        matchTimeText.text = matchTimeSlider.value.ToString();
        countdownText.text = countdownTimeSlider.value.ToString();

    }


    private void Load()
    {
        matchTimeSlider.value = PlayerPrefs.GetInt(settingsController.matchTimePrefs, settingsController.defaultMatchTime);
        countdownTimeSlider.value = PlayerPrefs.GetInt(settingsController.countdownPrefs, settingsController.defaultCountdown);
        SFXSlider.value = PlayerPrefs.GetFloat(settingsController.SFXVolumePrefs, settingsController.defaultSFXVolume);
        musicSlider.value = PlayerPrefs.GetFloat(settingsController.musicVolumePrefs, settingsController.defaultMusicVolume);
        
    }

    public void Save()
    {
        PlayerPrefs.SetInt(settingsController.matchTimePrefs, Mathf.RoundToInt(matchTimeSlider.value));
        PlayerPrefs.SetInt(settingsController.countdownPrefs, Mathf.RoundToInt(countdownTimeSlider.value));
        PlayerPrefs.SetFloat(settingsController.SFXVolumePrefs, SFXSlider.value);
        PlayerPrefs.SetFloat(settingsController.musicVolumePrefs, musicSlider.value);

    }

    public void DefaultValues()
    {
        PlayerPrefs.SetInt(settingsController.matchTimePrefs, settingsController.defaultMatchTime);
        PlayerPrefs.SetInt(settingsController.countdownPrefs, settingsController.defaultCountdown);
        PlayerPrefs.SetFloat(settingsController.SFXVolumePrefs, settingsController.defaultSFXVolume);
        PlayerPrefs.SetFloat(settingsController.musicVolumePrefs, settingsController.defaultMusicVolume);

        Load();
    }

}
