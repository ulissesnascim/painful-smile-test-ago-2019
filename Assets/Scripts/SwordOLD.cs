﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordOLD : MonoBehaviour
{
    private int damageGiven = 0;

    private bool newAttack = true;
    public bool _attackingFlag;

    void Start()
    {
        damageGiven = GetComponentInParent<EnemyCombatStats>().meleeDamage;
        _attackingFlag = GetComponentInParent<MeleeEnemy>().attackingFlag;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_attackingFlag)
            newAttack = true;
    }


    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            if (!GetComponentInParent<EnemyCombatStats>().enemyDied)
                PlayerHit(other);

        }

    }

    private void PlayerHit(Collider _other)
    {
        if (newAttack)
        {
            _other.gameObject.GetComponent<PlayerStats>().TakeDamage(damageGiven, transform);

            //to avoid damaging player twice in the same attack:
            newAttack = false;
            
        }         

    }
}


